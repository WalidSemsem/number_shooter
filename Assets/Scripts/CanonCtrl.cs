﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonCtrl : MonoBehaviour
{
    Vector3 mPrevPos = Vector3.zero;
    Vector3 mPosDelta = Vector3.zero;

    bool isDragging = false;
    bool isShooting = false;
    public Transform cannon;
    public GameObject bullet;
    GameObject firedBullet;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isDragging)
        {
            mPosDelta = Input.mousePosition - mPrevPos;
            if (cannon.rotation.eulerAngles.z < 61 || cannon.rotation.eulerAngles.z > 299)
            {
                cannon.Rotate(cannon.forward, -Vector3.Dot(mPosDelta, Camera.main.transform.right), Space.World);
            }
            if (cannon.rotation.eulerAngles.z > 60 && cannon.rotation.eulerAngles.z < 270)
                cannon.eulerAngles = new Vector3(0, 0, 60);
            else if (cannon.rotation.eulerAngles.z < 300 && cannon.rotation.eulerAngles.z > 180)
                cannon.eulerAngles = new Vector3(0, 0, 300);
        }
        mPrevPos = Input.mousePosition;
    }

    public void StartDrag()
    {
        isDragging = true;
    }

    public void StopDrag()
    {
        isDragging = false;
    }

    public void Fire()
    {
        firedBullet = Instantiate(bullet, cannon.GetChild(0).transform.position, cannon.transform.rotation) as GameObject;
        //firedBullet.transform.position = cannon.GetChild(0).transform.position;
        firedBullet.transform.parent = cannon.parent;
    }
}
